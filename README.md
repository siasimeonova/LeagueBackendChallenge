# Simple implementation of csv data transformer.

This project is simple REST service, that could be used for manipulation of csv data.

## Minor environment requirement 

php v.7.3, composer 

## Installation

Run `composer install` in the root folder

## Start the server

Run `symfony server:start` in the root folder

## Execute echo curl request

From the folder where the matrix.csv file is located run:

`curl -H 'Content-Type: text/csv' --data-binary @matrix.csv localhost:8000/echo`

## Run unit tests

./vendor/bin/phpunit tests

