<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Service\CsvTransformService;
use App\Service\ValidationService;
use Exception;
/**
 * Description of MainController
 *
 * @author sia.simeonova
 */
class CsvTransformController extends AbstractController {

    const ERROR_MESSAGE_EMPTY_REQUEST = 'The provided file is empty!';

    /**
     * @var CsvTransformService 
     */
    private $csvService;

    /**
     * @var ValidationService 
     */
    private $validationService;
    
    /**
     * 
     * @param CsvTransformService $service
     * @param ValidationService $validationService
     */
    public function __construct(CsvTransformService $service, ValidationService $validationService){
        $this->csvService = $service;
        $this->validationService = $validationService;
    }

    /**
     * @Route("/echo")
     *
     * @param Request $request
     *
     * @return Response
     */
    public function echo(Request $request):Response
    {
        return $this->processRequest($request, 'csvToStringMatrix');
    }

    /**
     * @Route("/invert")
     *
     * @param Request $request
     *
     * @return Response
     */
    public function invert(Request $request):Response
    {
        return $this->processRequest($request, 'csvToStringInvertedMatrix');
    }

    /**
     * @Route("/flatten")
     * 
     * @param Request $request
     *
     * @return Response
     */
    public function flatten(Request $request):Response
    {
        return $this->processRequest($request, 'csvToStringFlattenMatrix');
    }

    /**
     * @Route("/sum")
     * 
     * @param Request $request
     *
     * @return Response
     */
    public function sum(Request $request):Response
    {
        return $this->processRequest($request, 'csvToStringSumValues');
    }

    /**
     * @Route("/multiply")
     * 
     * @param Request $request
     *
     * @return Response
     */
    public function multiply(Request $request):Response
    {
        return $this->processRequest($request, 'csvToStringProductValues');
    }

    /**
     * General method for processing of request. 
     * Validates request and return the result of the given service method call 
     * or error response in case empty/invalid contents of the request.
     * 
     * @param Request $request
     *
     * @param string $method
     *
     * @return Response
     */
    private function processRequest(Request $request, string $method){
        try{
            $this->validateInputData($request);
            $result = call_user_func(array($this->csvService, $method), $request->getContent(true));
        }catch(Exception $ex){
            return new Response($ex->getMessage(), Response::HTTP_BAD_REQUEST); 
        }
        
        return new Response($result); 
    }

    /**
     * Validate if the request data met the expected requirements and can be processed.
     * 
     * @param type $request
     *
     * @throws Exception
     */
    private function validateInputData($request){
        if(empty($request->getContent())){
            throw new Exception(self::ERROR_MESSAGE_EMPTY_REQUEST);
        }

        $this->validationService->validateOnlyNumericValues($request->getContent());
        $this->validationService->validateIsMatrix($this->csvService->csvToArray($request->getContent(true)));
    }

}
