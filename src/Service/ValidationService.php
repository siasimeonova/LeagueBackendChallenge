<?php

namespace App\Service;

use Exception;

/**
 * Service that holds methods for validation of the input information.
 *
 * @author sia.simeonova
 */
class ValidationService {
    
    public const ERROR_MESSAGE_EMPTY_MATRIX = 'EMPTY OR BROKEN FILE!';
        
    public const ERROR_MESSAGE_NOT_ALLOWED_VALUE = 'NOT EXPECTED VALUES IN THE FILE ONLY INTEGERS ARE ALLOWED!';
    
    public const ERROR_MESSAGE_NOT_MATRIX = 'DATA IS NOT A VALID MATRIX!';
    
    public const ERROR_MESSAGE_EMPTY_CELLS = 'DATA IS NOT A VALID MATRIX! EMPTY CELLS ARE NOT ALLOWED!';
    
    public const MATCH_DIGITS_REG_EX = '/^[0-9]+$/';
    
    public const SYMBOLS_NEW_LINE_OR_COMMA = array("\n\r", "\n", "\r", ",");
    
    /**
     * 
     * @param type $data
     *
     * @return void
     * 
     * @throws Exception
     */
    public function validateOnlyNumericValues($data): void
    {
        $values = str_replace(self::SYMBOLS_NEW_LINE_OR_COMMA, '', $data);
        if(empty($values)){
            throw new Exception(self::ERROR_MESSAGE_EMPTY_MATRIX);
        }
        
        if(preg_match(self::MATCH_DIGITS_REG_EX, $values) !== 1){

            throw new Exception(self::ERROR_MESSAGE_NOT_ALLOWED_VALUE);
        }
    }
    
    /**
     * Validate if the given multydimentional array is a matrix.
     * 
     * @param array $matrix
     * 
     * @return void
     * 
     * @throws Exception
     */

    public function validateIsMatrix(array $matrix): void
    {
        if(empty($matrix)){
            throw new Exception(self::ERROR_MESSAGE_EMPTY_MATRIX);
        }

        $rowsCount = count($matrix);
        foreach ($matrix as $rows){
            if(!is_array($rows) && $rowsCount == 1){
                return; // a 1x1 matrix is square so it should be acceptable.
            }

            if(count($rows) != $rowsCount){
                throw new Exception(self::ERROR_MESSAGE_NOT_MATRIX);
            }
        }
    }
}
