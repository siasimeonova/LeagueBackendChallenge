<?php

namespace App\Service;

use App\Service\ValidationService;
use Exception;

/**
 * Service class that provide convenient methods for csv operations
 *
 * @author sia.simeonova
 */
class CsvTransformService {
    
    /**
     * Transform the stream to string representation withoud changing the elements.
     *
     * @param type $handle
     *
     * @return string The string representation of the given stream.
     */
    public function csvToStringMatrix($handle): string
    {
        $matrix = '';
        while (($data = fgetcsv($handle)) !== false) {
            $arraySize = count($data);
            for ($index=0; $index < $arraySize; $index++) {
                $matrix .= $data[$index] . ',';
            }
            $matrix = substr($matrix, 0, -1) . PHP_EOL;
        }
        fclose($handle);
    
        return rtrim($matrix);
    }
    
    /**
     * Transforn the stream to string.
     *
     * @param type $handle
     *
     * @return string the transformed as a string values
     */
    public function csvToStringFlattenMatrix($handle): string
    {
        $matrix = '';
        while (($data = fgetcsv($handle)) !== false) {
            $arraySize = count($data);
            for ($index=0; $index < $arraySize; $index++) {
                $matrix .= $data[$index] . ',';
            }
        }
        fclose($handle);
    
        return substr($matrix, 0, -1);
    }
    
    /**
     * Sum all numeric values in the given stream and return the result.
     *
     * @param type $handle
     *
     * @return string The sum of the values
     */
    public function csvToStringSumValues($handle): string
    {
        $sum = 0;
        while (($data = fgetcsv($handle)) !== false) {
            $arraySize = count($data);
            for ($index=0; $index < $arraySize; $index++) {
                $sum += $data[$index];
            }
        }
        fclose($handle);
    
        return $sum;
    }
    
    /**
     * Multiply all numeric values in the matrix tream and return the product of the multiplication.
     * 
     * @param type $handle
     *
     * @return  string The product of the values
     */
    public function csvToStringProductValues($handle): string
    {
        $product = 1;
        while (($data = fgetcsv($handle)) !== false) {
            $arraySize = count($data);
            for ($index=0; $index < $arraySize; $index++) {
                $product *= $data[$index];
            }
        }
        fclose($handle);
    
        return $product;
    }

    /**
     * Converts csv stream to mylty-dimentional array.
     *
     * @param type $handle
     *
     * @return array
     *
     * @throws Exception
     */
    public function csvToArray($handle): array
    {
        $matrix = array();
        while (($data = fgetcsv($handle)) !== false) {
            $arraySize = count($data);
            $rowData = array();
            for ($index=0; $index < $arraySize; $index++) {
                if(!is_numeric($data[$index]) && empty($data[$index])){
                    throw new Exception(ValidationService::ERROR_MESSAGE_EMPTY_CELLS);
                }
                 $rowData[] = $data[$index];
            }
            $matrix[] = $rowData;
        }
        fclose($handle);

        return $matrix;
    }
    
    /**
     * Create invered representation of the matrix.
     *
     * @param type $handle
     *
     * @return string
     */
    public function csvToStringInvertedMatrix($handle): string
    {
        $matrix = $this->csvToArray($handle);
        if(empty($matrix)){
            throw new Exception(ValidationService::ERROR_MESSAGE_EMPTY_MATRIX);
        }

        $matrixInverted = '';

        foreach ($matrix as $index => $row)
        {
            $keys = array_keys($row);
            foreach ($keys as $key)
            {
                $matrixInverted .= $matrix[$key][$index] . ',';
            }

            $matrixInverted = substr($matrixInverted, 0, -1) . PHP_EOL;
        }
    
        return rtrim($matrixInverted);
    }

}
