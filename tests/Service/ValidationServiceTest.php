<?php

namespace App\tests\Service;

use Exception;
use PHPUnit\Framework\TestCase;
use App\Service\ValidationService;

/**
 * Tests for ValidationService class
 *
 * @author sisi
 */
class ValidationServiceTest extends TestCase {
   
    /**
     * @var ValidationService
     */
    private $testObject;
    
    /**
     * @SetUp
     */
    protected function setUp(): void
    {
        $this->testObject = new ValidationService();
    }

    /**
     * Verify that in case invalid data string error message will be thrown.
     * 
     * @test
     * @dataProvider validateOnlyNumericValuesDataProvider
     * 
     * @param string $data
     * @param string $expectedErrorMessage
     * @return void
     */
    public function testValidateOnlyNumericValues(string $data, string $expectedErrorMessage) : void
    {
        if($expectedErrorMessage){
            $this->expectException(Exception::class);
            $this->expectExceptionMessage($expectedErrorMessage);
            
            $this->testObject->validateOnlyNumericValues($data);
        }else{
            try{
                 $this->testObject->validateOnlyNumericValues($data);
                 $this->assertTrue(true);
            } catch (Exception $ex) {
                $this->fail('Not exception exception in "' . __METHOD__ . '" method. Error [' . $ex->getMessage() . '].');
            }
        }
    }
    
    /**
     * Provider for testValidateOnlyNumericValues method
     * @dataProvider
     * 
     * @return array
     */
    public function validateOnlyNumericValuesDataProvider() : array
    {
        return [
            'empty csv' => ['', ValidationService::ERROR_MESSAGE_EMPTY_MATRIX],
            'correct csv' => ["1,2,3\n4,5,6\n7,8,9", ''],
            'correct csv one element' => ['1', ''],
            'error a not allowed symbol letter' => ['1,2,a', ValidationService::ERROR_MESSAGE_NOT_ALLOWED_VALUE],
            'error float value' => ["1,2,3\n4,'5.5',6", ValidationService::ERROR_MESSAGE_NOT_ALLOWED_VALUE],
            'error not allowed symbol @' => ["1,2,3\n4,5,@", ValidationService::ERROR_MESSAGE_NOT_ALLOWED_VALUE]
        ];
    }
    
    
    /**
     * Verify that in case invalid matrix error message will be thrown.
     * 
     * @test
     * @dataProvider validateIsMatrixDataProvider
     * 
     * @param array $array
     * @param bool $expectedErrorMessage
     */
    public function testValidateIsMatrix(array $array, string $expectedErrorMessage) : void
    {
        if($expectedErrorMessage){
            $this->expectException(Exception::class);
            $this->expectExceptionMessage($expectedErrorMessage);
     
            $this->testObject->validateIsMatrix($array);
        }else{
            try{
                 $this->testObject->validateIsMatrix($array);
                 $this->assertTrue(true);
            } catch (Exception $ex) {
                $this->fail('Not exception exception in "' . __METHOD__ . '" method. Error [' . $ex->getMessage() . '].');
            }
        }
    }
    
    /**
     * Provider for testValidateIsMatrix method
     * @dataProvider
     * 
     * @return array
     */
    public function validateIsMatrixDataProvider(): array
    {
        return [
            'empty array' => [[], ValidationService::ERROR_MESSAGE_EMPTY_MATRIX],
            'correct csv one element' => [[1], ''],
            'correct array' => [[[1,2,3],[4,5,6], [7,8,9]], ''],
            'correct two dimentional array' => [[[1,2],[3,4]], ''],
            'error one dimentional array' => [[[1,2,3]], ValidationService::ERROR_MESSAGE_NOT_MATRIX],
            'error mylti dimentional array' => [[[1,2,3], [4,5,6], [7]], ValidationService::ERROR_MESSAGE_NOT_MATRIX],
            'error mylti dimentional array mixed errors' => [[[1,2,3], [4,5,6,7,8], [9]], ValidationService::ERROR_MESSAGE_NOT_MATRIX]
        ];
    }
}
