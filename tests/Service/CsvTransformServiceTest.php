<?php

namespace App\tests\Service;

use Exception;
use PHPUnit\Framework\TestCase;
use App\Service\CsvTransformService;

/**
 * Tests for CsvTransformServiceTest class
 *
 * @author sisi
 */
class CsvTransformServiceTest extends TestCase {

    /**
     * @var CsvTransformService
     */
    private $testObject;

    /**
     * @SetUp
     */
    protected function setUp(): void
    {
        $this->testObject = new CsvTransformService();
    }

    /**
     * @test
     * @dataProvider csvToStringMatrixDataProvider
     * 
     * @param type $string
     * @param string $expected
     * @return void
     */
    public function testCsvToStringMatrix(string $data) : void
    {
        try{
            $stream = fopen(sprintf('data://text/plain,%s', $data), 'r');
            $result = $this->testObject->csvToStringMatrix($stream);
            $this->assertEquals($data, $result);
        } catch (Exception $ex) {
            $this->fail('Not exception exception in "' . __METHOD__ . '" method. Error [' . $ex->getMessage() . '].');
        }
    }

    /**
     * Provider for testCsvToStringMatrix test
     * 
     * @return array
     */
    public function csvToStringMatrixDataProvider() : array
    {
        return [
            'one row' => ['1'],
            'two rows' => ['1,2' . PHP_EOL . '3,4'],
            'three rows' => ['1,2,3' . PHP_EOL . '4,5,6' . PHP_EOL . '7,8,9'],
        ];
    }

    /**
     * @test
     * @dataProvider csvToStringFlattenMatrixDataProvider
     * 
     * @param string $string
     * @param string $expected
     * @return void
     */
    public function testCsvToStringFlattenMatrix(string $string, string $expected) : void
    {
        try{
            $stream = fopen(sprintf('data://text/plain,%s', $string), 'r');
            $result = $this->testObject->csvToStringFlattenMatrix($stream);
            $this->assertEquals($expected, $result);
        } catch (Exception $ex) {
            $this->fail('Not exception exception in "' . __METHOD__ . '" method. Error [' . $ex->getMessage() . '].');
        }
    }

    /**
     * Provider for testCsvToStringFlattenMatrix test
     * 
     * @return array
     */
    public function csvToStringFlattenMatrixDataProvider() : array
    {
        return [
            'one row' => ['1', '1'],
            'two rows' => ['1,2' . PHP_EOL . '3,4', '1,2,3,4'],
            'three rows' => ['1,2,3' . PHP_EOL . '4,5,6' . PHP_EOL . '7,8,9', '1,2,3,4,5,6,7,8,9'],
        ];
    }

    /**
     * @test
     * @dataProvider csvToStringSumValuesDataProvider
     * 
     * @param string $string
     * @param string $expected
     * @return void
     */
    public function testCsvToStringSumValues(string $string, string $expected) : void
    {
        try{
            $stream = fopen(sprintf('data://text/plain,%s', $string), 'r');
            $result = $this->testObject->csvToStringSumValues($stream);
            $this->assertEquals($expected, $result);
        } catch (Exception $ex) {
            $this->fail('Not exception exception in "' . __METHOD__ . '" method. Error [' . $ex->getMessage() . '].');
        }
    }

    /**
     * Provider for testCsvToStringSumValues test
     * 
     * @return array
     */
    public function csvToStringSumValuesDataProvider() : array
    {
        return [
            'one row' => ['1', '1'],
            'two rows' => ['1,2' . PHP_EOL . '3,4', '10'],
            'three rows' => ['1,2,3' . PHP_EOL . '4,5,6' . PHP_EOL . '7,8,9', '45'],
            'sum zeroes' => ['0,0,0' . PHP_EOL . '0,0,0' . PHP_EOL . '0,0,0', '0'],
            'sum max integer' => ['1,2,9223372036854775807', '9.2233720368548E+18']
        ];
    }

    /**
     * @test
     * @dataProvider csvToStringProductValuesDataProvider
     * 
     * @param string $string
     * @param string $expected
     * @return void
     */
    public function testCsvToStringProductValues(string $string, string $expected) : void
    {
        try{
            $stream = fopen(sprintf('data://text/plain,%s', $string), 'r');
            $result = $this->testObject->csvToStringProductValues($stream);
            $this->assertEquals($expected, $result);
        } catch (Exception $ex) {
            $this->fail('Not exception exception in "' . __METHOD__ . '" method. Error [' . $ex->getMessage() . '].');
        }
    }

    /**
     * Provider for testCsvToStringProductValues test
     * 
     * @return array
     */
    public function csvToStringProductValuesDataProvider() : array
    {
        return [
            'one row' => ['1', '1'],
            'two rows' => ['1,2' . PHP_EOL . '3,4', '24'],
            'three rows' => ['1,2,3' . PHP_EOL . '4,5,6' . PHP_EOL . '7,8,9', '362880'],
            'multiply zeroes' => ['0,0,0' . PHP_EOL . '0,0,0' . PHP_EOL . '0,0,0', '0'],
            'multiply by zero' => ['1,2,3' . PHP_EOL . '4,5,6' . PHP_EOL . '7,8,0', '0'],
            'multiply by max integer' => ['9223372036854775807,9223372036854775807,9223372036854775807', '7.8463771692334E+56']
        ];
    }

    /**
     * @test
     * @dataProvider csvToArrayDataProvider
     * 
     * 
     * @param string $string
     * @param array $expected
     * @return void
     */
    public function testCsvToArray(string $string, array $expected): void
    {
        try{
            $stream = fopen(sprintf('data://text/plain,%s', $string), 'r');
            $result = $this->testObject->csvToArray($stream);

            $this->assertEquals($expected, $result);
        } catch (Exception $ex) {
            $this->fail('Not exception exception in "' . __METHOD__ . '" method. Error [' . $ex->getMessage() . '].');
        }
    }

    /**
     * Provider for testCsvToArray test
     * 
     * @return array
     */
    public function csvToArrayDataProvider() : array
    {
        return [
            'one value' => ['1', [[1]]],
            'one row' => ['1,2', [[1,2]]],
            'two rows' => ['1,2' . PHP_EOL . '3,4', [[1,2],[3,4]]],
            'three rows' => ['1,2,3' . PHP_EOL . '4,5,6' . PHP_EOL . '7,8,9', [[1,2,3],[4,5,6],[7,8,9]]]
        ];
    }

    /**
     * @test
     * @dataProvider csvToStringInvertedMatrixDataProvider
     * 
     * 
     * @param string $string
     * @param array $expected
     * @return void
     */
    public function testCsvToStringInvertedMatrix(string $string, string $expected): void
    {
        try{
            $stream = fopen(sprintf('data://text/plain,%s', $string), 'r');
            $result = $this->testObject->csvToStringInvertedMatrix($stream);
            $this->assertEquals($expected, $result);
        } catch (Exception $ex) {
            $this->fail('Not exception exception in "' . __METHOD__ . '" method. Error [' . $ex->getMessage() . '].');
        }
    }

    /**
     * Provider for testCsvToStringInvertedMatrix test
     * 
     * @return array
     */
    public function csvToStringInvertedMatrixDataProvider() : array
    {
        return [
            'one row' => ['1', '1'],
            'two rows' => ['1,2' . PHP_EOL . '3,4', '1,3'. PHP_EOL . '2,4'],
            'three rows' => ['1,2,3' . PHP_EOL . '4,5,6' . PHP_EOL . '7,8,9', '1,4,7' . PHP_EOL . '2,5,8' . PHP_EOL . '3,6,9']
        ];
    }
}
