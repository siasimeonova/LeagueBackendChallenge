<?php

namespace App\tests\Controller;

use App\Controller\CsvTransformController;
use App\Service\CsvTransformService;
use App\Service\ValidationService;
use Exception;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Test methods in CsvTransformControllerTest
 *
 * @author sia.simeonova
 */
class CsvTransformControllerTest extends TestCase{
    
    const ECHO_RESULT = '1,2' . PHP_EOL . '3,4';

    const INVERT_RESULT = '1,3' . PHP_EOL . '2,4';

    const FLATTEN_RESULT = '1,2,3,4';

    const SUM_RESULT = '10';

    const MULTIPLY_RESULT = '24';
    
    
    /**
     *
     * @var CsvTransformController 
     */
    private $testee;
    
    /**
     * @SetUp
     */
    protected function setUp(): void
    {
        $validationServiceMock = $this->createMock(ValidationService::class);
        $ssvTransformService = $this->createMock(CsvTransformService::class);

        $ssvTransformService->method('csvToStringInvertedMatrix')
                            ->willReturn(self::INVERT_RESULT);

        $ssvTransformService->method('csvToStringMatrix')
                            ->willReturn(self::ECHO_RESULT);

        $ssvTransformService->method('csvToStringFlattenMatrix')
                            ->willReturn(self::FLATTEN_RESULT);

        $ssvTransformService->method('csvToStringSumValues')
                            ->willReturn(self::SUM_RESULT);

        $ssvTransformService->method('csvToStringProductValues')
                            ->willReturn(self::MULTIPLY_RESULT);       
        
        $this->testee = new CsvTransformController($ssvTransformService, $validationServiceMock);
    }

    /**
     * @test
     */
    public function testEcho(){
        $responseResult = $this->testee->echo($this->getDummyRequest());
        $this->assertEquals(Response::HTTP_OK, $responseResult->getStatusCode());
        $this->assertEquals(self::ECHO_RESULT, $responseResult->getContent());
    }

    /**
     * @test
     */
    public function testInvert(){
        $responseResult = $this->testee->invert($this->getDummyRequest());
        $this->assertEquals(Response::HTTP_OK, $responseResult->getStatusCode());
        $this->assertEquals(self::INVERT_RESULT, $responseResult->getContent());
    }

    /**
     * @test
     */
    public function testFlatten(){
        $responseResult = $this->testee->flatten($this->getDummyRequest());
        $this->assertEquals(Response::HTTP_OK, $responseResult->getStatusCode());
        $this->assertEquals(self::FLATTEN_RESULT, $responseResult->getContent());
    }

    /**
     * @test
     */
    public function testSum(){
        $responseResult = $this->testee->sum($this->getDummyRequest());
        $this->assertEquals(Response::HTTP_OK, $responseResult->getStatusCode());
        $this->assertEquals(self::SUM_RESULT, $responseResult->getContent());
    }

    /**
     * @test
     */
    public function testMultiply(){
        $responseResult = $this->testee->multiply($this->getDummyRequest());
        $this->assertEquals(Response::HTTP_OK, $responseResult->getStatusCode());
        $this->assertEquals(self::MULTIPLY_RESULT, $responseResult->getContent());
    }

    /**
     * @test
     */
    public function testEchoInvalidRequestData(){
        $validationServiceMock = $this->createMock(ValidationService::class);
        $ssvTransformService = $this->createMock(CsvTransformService::class);

        $validationServiceMock->method('validateOnlyNumericValues')
                            ->willThrowException(new Exception(ValidationService::ERROR_MESSAGE_EMPTY_MATRIX));

        $testee = new CsvTransformController($ssvTransformService, $validationServiceMock);

        $responseResult = $testee->echo($this->getDummyRequest());
        $this->assertEquals(Response::HTTP_BAD_REQUEST, $responseResult->getStatusCode());
        $this->assertEquals(ValidationService::ERROR_MESSAGE_EMPTY_MATRIX, $responseResult->getContent());
    }
    
    /**
     * Create mock of Symfony\Component\HttpFoundation\Request for test purposes.
     * 
     * @return Request
     */
    private function getDummyRequest() : Request
    {
        $request = $this->createMock(Request::class);
        $request->method('getContent')
                ->willReturn(fopen(sprintf('data://text/plain,%s', self::ECHO_RESULT), 'r'));
        
        return $request;
    }
}
